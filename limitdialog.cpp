/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include <QFormLayout>
#include <QBoxLayout>
#include <QLineEdit>
#include <QPushButton>

#include "limitdialog.h"

LimitDialog::LimitDialog(QWidget *parent) : QDialog(parent),
    m_min(new QLineEdit(this)),
    m_max(new QLineEdit(this))
{
    QFormLayout *layout=new QFormLayout(this);
    layout->addRow("Min", m_min);
    layout->addRow("Max", m_max);
    QBoxLayout *buttonLayout=new QBoxLayout(QBoxLayout::LeftToRight);
    layout->addRow(buttonLayout);
    QPushButton *ok=new QPushButton(this);
    QPushButton *cancel=new QPushButton(this);
    buttonLayout->addWidget(ok);
    buttonLayout->addWidget(cancel);
    ok->setText("&Ok");
    cancel->setText("&Cancel");
    connect(cancel, SIGNAL(clicked(bool)), this, SLOT(close()));
    connect(ok, &QPushButton::clicked, [=]{emit requireSet(m_min->text().toInt(), m_max->text().toInt());close();});
    setLayout(layout);
}
