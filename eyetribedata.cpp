/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include <QApplication>
#include <QPainter>
#include <cmath>

#include "eyetribedata.h"

#include <QDebug>

#define mmax(a, b)  (a)>(b)?(a):(b)
#define mmin(a, b)  (a)<(b)?(a):(b)

EyetribeData::EyetribeData(QObject *parent) : QObject(parent),
    m_x(0),
    m_y(0),
    m_rangeSize(0),
    m_counter(nullptr),
    m_range(nullptr)
{
}

EyetribeData::~EyetribeData()
{
    clearCounter();
    clearRange();
}

void EyetribeData::initial(int x, int y)
{
    clearCounter();
    m_x = x;
    m_y = y;
    m_counter=new int *[x];
    for(int i=0; i<x; ++i)
    {
        m_counter[i]=new int[y];
    }
    //Reset all of the number to 0.
    for(int i=0; i<x; ++i)
    {
        for(int j=0; j<y; ++j)
        {
            m_counter[i][j]=0;
        }
    }
}

void EyetribeData::setRange(int range)
{
    clearRange();
    m_rangeSize=range;
    m_range=new int[range];
    double sq2=sqrt(2.0), previous=0.0;
    for(int i=0; i<range; ++i)
    {
        double xi=(double)(i)/(double)range,
                current=(0.5 + 0.5 * erf((xi/sq2)*3)) / 0.5;
        m_range[i]=(current-previous)*100.0;
        previous=current;
    }
}

void EyetribeData::increase(int x, int y)
{
    if(x==0 && y==0)
    {
        return;
    }
    if(x < m_x && y < m_y && x > -1 && y > -1)
    {
        //Loop and check.
        for(int i=mmax(x-m_rangeSize, 0), i_max=mmin(x+m_rangeSize, m_x); i<i_max; ++i)
        {
            for(int j=mmax(y-m_rangeSize, 0), j_max=mmin(y+m_rangeSize, m_y); j<j_max; ++j)
            {
                int d_i=(double)(i-x), d_j=(double)(j-y);
                int length=(int)sqrt(d_i*d_i+d_j*d_j);
                if(length<m_rangeSize)
                {
                    m_counter[i][j]+=m_range[length];
                }
            }
        }
    }
}

void EyetribeData::render()
{
    if(m_x<1 || m_y<1)
    {
        return;
    }
    m_result=QImage(m_x, m_y, QImage::Format_ARGB32);
    m_result.fill(QColor(0, 0, 0, 0));
    int counter=0;
    double rangeAll=(double)(m_maxAll-m_minAll);
    for(int i=0; i<m_x; ++i)
    {
        for(int j=0; j<m_y; ++j)
        {
            if(m_counter[i][j]==0 || m_counter[i][j]<m_minAll)
            {
                continue;
            }
            ++counter;
            double pValue=(double)(m_counter[i][j]-m_minAll)/rangeAll;
            QColor pColor;
            pColor.setHsvF((1.0-pValue)*0.72, 1.0, 1.0, 1.0);
            m_result.setPixelColor(i, j, pColor);
        }
    }

    // Draw a side thing.
    QPainter painter(&m_result);
    // Draw the max point.
    painter.drawEllipse(QPointF(m_maxX, m_maxY), 10, 10);
    int fontHeight=painter.fontMetrics().height();
    painter.setPen(Qt::black);
    QLinearGradient meter(0, 0, 0, 50+(fontHeight>>1));
    meter.setColorAt(0, QColor(255, 0, 0));
    meter.setColorAt(0.23, QColor(255, 255, 0));
    meter.setColorAt(0.46, QColor(0,255,0));
    meter.setColorAt(0.69, QColor(0,255,255));
    meter.setColorAt(0.92, QColor(0,0,255));
    meter.setColorAt(1, QColor(85,0,255));
    painter.setBrush(meter);
    painter.drawRect(20, 0+(fontHeight>>1), 20, 50);
    painter.drawText(50, 0+painter.fontMetrics().height(), QString::number(m_maxAll));
    painter.drawText(50, 50+painter.fontMetrics().height(), QString::number(m_minAll));
    painter.end();
}

void EyetribeData::statistic()
{
    m_minAll=0; m_maxAll=-1;
    for(int i=0; i<m_x; ++i)
    {
        for(int j=0; j<m_y; ++j)
        {
            if(m_counter[i][j]>m_maxAll)
            {
                m_maxAll=m_counter[i][j];
                m_maxX=i;
                m_maxY=j;
            }
            if(m_counter[i][j]<m_minAll || m_minAll==0)
            {
                m_minAll=m_counter[i][j];
            }
        }
    }
}

void EyetribeData::setMinMax(int m1, int m2)
{
    if(m1 < 0 || m2 < 0 || m2 <= m1)
    {
        return;
    }
    m_minAll = m1;
    m_maxAll = m2;
}

void EyetribeData::clearRange()
{
    if(m_range)
    {
        delete[] m_range;
    }
}

void EyetribeData::clearCounter()
{
    if(m_counter)
    {
        //Delete lines.
        for(int i=0; i<m_x; ++i)
        {
            delete[] m_counter[i];
        }
        delete[] m_counter;
    }
}

QImage EyetribeData::result() const
{
    return m_result;
}
