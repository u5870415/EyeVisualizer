/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#ifndef EYETRIBEDATA_H
#define EYETRIBEDATA_H

#include <QImage>

#include <QObject>

class EyetribeData : public QObject
{
    Q_OBJECT
public:
    explicit EyetribeData(QObject *parent = 0);
    ~EyetribeData();

    void initial(int x, int y);

    void setRange(int range);

    void increase(int x, int y);

    void render();
    void statistic();

    void setMinMax(int m1, int m2);

    QImage result() const;

signals:

public slots:

private:
    inline void clearRange();
    inline void clearCounter();
    QImage m_result;
    int m_x, m_y, m_rangeSize, m_minAll, m_maxAll, m_maxX, m_maxY;
    int **m_counter;
    int *m_range;
};

#endif // EYETRIBEDATA_H
