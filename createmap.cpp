/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include <QFormLayout>
#include <QLineEdit>
#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>

#include "createmap.h"

CreateMap::CreateMap(QWidget *parent) : QDialog(parent)
{
    QFormLayout *mainLayout=new QFormLayout(this);
    QLineEdit *widthEdit=new QLineEdit(this), *heightEdit=new QLineEdit(this),
            *spotSize=new QLineEdit(this);
    mainLayout->addRow(new QLabel("Graphics Setting"));
    mainLayout->addRow("Width", widthEdit);
    mainLayout->addRow("Height", heightEdit);
    mainLayout->addRow("Sight Spot", spotSize);
    QBoxLayout *operationLayout=new QBoxLayout(QBoxLayout::LeftToRight);
    QPushButton *okay=new QPushButton("&Ok", this);
    operationLayout->addWidget(okay);
    QPushButton *cancel=new QPushButton("&Cancel", this);
    connect(cancel, SIGNAL(clicked(bool)), this, SLOT(close()));
    operationLayout->addWidget(cancel);
    mainLayout->addRow(operationLayout);
    setLayout(mainLayout);
    connect(okay, &QPushButton::clicked, [=]{
        emit createMap(widthEdit->text().toInt(),
                       heightEdit->text().toInt(),
                       spotSize->text().toInt());
        close();
    });
}
