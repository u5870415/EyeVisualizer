/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QPixmap>

#include <QMainWindow>

class QLabel;
class LimitDialog;
class CreateMap;
class EyetribeData;
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:

public slots:

private slots:
    void onCreate();
    void onImport();
    void createMap(int x, int y, int size);
    void updateGraphics(int m1, int m2);

private:
    void rerender();
    EyetribeData *m_data;
    CreateMap *m_createMap;
    LimitDialog *m_limit;
    QPixmap m_resultPixmap;
    QLabel *m_display;
};

#endif // MAINWINDOW_H
