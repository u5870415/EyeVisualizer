QT += core gui widgets
CONFIG += c++11

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    eyetribedata.cpp \
    createmap.cpp \
    limitdialog.cpp

HEADERS += \
    mainwindow.h \
    eyetribedata.h \
    createmap.h \
    limitdialog.h

