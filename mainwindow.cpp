/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include <QMenu>
#include <QMenuBar>
#include <QApplication>
#include <QLabel>
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "createmap.h"
#include "eyetribedata.h"
#include "limitdialog.h"

#include "mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    m_data(nullptr),
    m_createMap(new CreateMap(this)),
    m_limit(new LimitDialog(this)),
    m_display(new QLabel(this))
{
    QApplication::setApplicationDisplayName("Eye Visualizer");
    m_display->setAutoFillBackground(true);
    m_display->setScaledContents(true);
    setCentralWidget(m_display);
    QMenu *fileMenu=new QMenu("&File", this);
    menuBar()->addMenu(fileMenu);

    QAction *create=new QAction("&New", this);
    create->setShortcut(QKeySequence::New);
    fileMenu->addAction(create);
    connect(create, SIGNAL(triggered(bool)), this,  SLOT(onCreate()));
    QAction *importData=new QAction("&Import...", this);
    importData->setShortcut(QKeySequence::Open);
    fileMenu->addAction(importData);
    connect(importData, SIGNAL(triggered(bool)), this, SLOT(onImport()));
    m_createMap->hide();
    connect(m_createMap, &CreateMap::createMap, this, &MainWindow::createMap, Qt::QueuedConnection);
    QAction *setLimit = new QAction("S&et max and min...", this);
    fileMenu->addAction(setLimit);
    connect(setLimit, SIGNAL(triggered(bool)), m_limit, SLOT(exec()));
    connect(m_limit, SIGNAL(requireSet(int,int)), this, SLOT(updateGraphics(int,int)));
    QAction *saveAs = new QAction("&Export...", this);
    saveAs->setShortcut(QKeySequence::Save);
    fileMenu->addAction(saveAs);
    connect(saveAs, &QAction::triggered, [=]
    {
        QString savePath=QFileDialog::getSaveFileName(this, "Eyetribe Data");
        if(savePath.isEmpty())
        {
            return;
        }
        m_resultPixmap.save(savePath, "png", 100);
    });
}

MainWindow::~MainWindow()
{
    if(m_data)
    {
        m_data->deleteLater();
    }
}

void MainWindow::onCreate()
{
    m_createMap->exec();
}

void MainWindow::onImport()
{
    QString filePath=QFileDialog::getOpenFileName(this, "Eyetribe Data");
    if(filePath.isEmpty())
    {
        return;
    }
    QFile dataFile(filePath);
    if(!dataFile.exists())
    {
        QMessageBox::warning(this, "Error", "File not exist.", QMessageBox::Ok);
        return;
    }
    if(!dataFile.open(QIODevice::ReadOnly))
    {
        QMessageBox::warning(this, "Error", "Failed to open file.", QMessageBox::Ok);
        return;
    }
    if(!m_data)
    {
        return;
    }
    QString rawData=dataFile.readAll();
    dataFile.close();
    QStringList pointData=rawData.split("\n");
    for(int i=1; i<pointData.size(); ++i)
    {
        QStringList pointData = pointData[i].split(",");
        if(pointData.size()<2)
        {
            continue;
        }
        m_data->increase(pointData[0].toDouble(), pointData[1].toDouble());
    }
    m_data->statistic();
    rerender();
}

void MainWindow::createMap(int x, int y, int size)
{
    if(m_data)
    {
        m_data->deleteLater();
    }
    m_data=new EyetribeData;
    m_data->initial(x, y);
    m_data->setRange(size);
    setWindowTitle("New Document");
}

void MainWindow::updateGraphics(int m1, int m2)
{
    if(!m_data)
    {
        return;
    }
    m_data->setMinMax(m1, m2);
    rerender();
}

void MainWindow::rerender()
{
    m_data->render();
    m_resultPixmap=QPixmap::fromImage(m_data->result());
    m_display->setPixmap(m_resultPixmap);
}
